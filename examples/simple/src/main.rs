use css_style::{color, unit::px, Background, Border};
use yew::prelude::*;
use yew_layout::{
    Align, AlignRows, Column, CrossAlign, Gap, Length, Margin, Overflow, Padding, Row,
};

#[function_component(App)]
pub fn app() -> Html {
    let border = Border::from(color::named::DARKORCHID).width(px(2)).dashed();
    html! {
        <Column align={ Align::Center }
                cross_align={ CrossAlign::Center }
                gap={ Gap::from(px(8)) }
                width={ Length::from(px(350)) }
                length={ Length::from(px(550)) }
                border={ Border::default().radius(px(8)) }
                background={ Background::from(color::named::SILVER) } >
            <Row align={ Align::Center }
                 align_rows={ AlignRows::Center }
                 wrap=true
                 min_width={ Length::from(px(220)) }
                 gap={ Gap::from(px(12)) }
                 padding={ Padding::default().y(px(12)) }
                 margin={ Margin::default().x(px(20)) }
                 border={ border.clone().top_left(px(8)).top_right(px(8)) }>
                {
                    (0..=11)
                        .into_iter()
                        .map(|i| html!{
                            <div key={i} style={ "background: dodgerblue; height: 40px; width: 40px;" }></div>
                        }).collect::<Html>()
                }
            </Row>
            <Column border={ border.bottom_left(px(8)).bottom_right(px(8)) }
                    overflow={ Overflow::hidden().y_auto_scroll() }
                    padding={ Padding::default().x(px(10)) }>
                <h1>{ "Sub-column with scroll bar" }</h1>
                <p>{ "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit tempor tellus.  Donec pretium posuere tellus.  Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus.  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  Nulla posuere.  Donec vitae dolor.  Nullam tristique diam non turpis.  Cras placerat accumsan nulla.  Nullam rutrum.  Nam vestibulum accumsan nisl." }</p>
            </Column>
        </Column>
    }
}

fn main() {
    yew::start_app::<App>();
}
