use crate::*;
use css_style::{
    box_align::*, flexbox::*, prelude::*, size, Background, Border, Display, FlexDirection,
};
use yew::prelude::*;

// ======================== Row Component ========================

/// Row properties.
///
/// Here you can find all properties that can be used with
/// [Row](crate::row::Row) component.
#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    pub children: Children,

    /// Control the length of the Row
    ///
    /// The default is `Length::from(1.0)`
    #[prop_or(Some(size::Length::from(1.0)))]
    pub length: Option<Length>,

    /// Control the minimum length of the Row
    ///
    /// The default is `Length::MinContent`
    #[prop_or(Some(size::Length::MinContent))]
    pub min_length: Option<Length>,

    /// Control the maximum length of the Row
    ///
    /// The default is `None`
    #[prop_or_default]
    pub max_length: Option<Length>,

    /// Control the width of the Row
    ///
    /// The default is `None`
    #[prop_or_default]
    pub width: Option<Length>,

    /// Control the minimum width of the Row
    ///
    /// The default is `Length::MinContent`
    #[prop_or(Some(size::Length::MinContent))]
    pub min_width: Option<Length>,

    /// Control the maximum width of the Row
    ///
    /// The default is `None`
    #[prop_or_default]
    pub max_width: Option<Length>,

    /// Expand factor used to expand this row in direction relevant to it's
    /// parent layout direction.
    ///
    /// When the parent is `Row` it will expand horizontally, when the parent is
    /// `Column` it will expand vertically.
    ///
    /// Note: This only works when this `Row` inside another layout (e.g. Row/Column).
    ///
    /// The default is `None`
    #[prop_or_default]
    pub expand_by: Option<f32>,

    /// Shrink factor used to shrink this row in direction relevant to it's
    /// parent layout direction when needed.
    ///
    /// When the parent is `Row` it will shrink horizontally, when the parent is
    /// `Column` it will shrink vertically.
    ///
    /// Note: This only works when this `Row` inside another layout (e.g. Row/Column).
    ///
    /// The default is `None`
    #[prop_or_default]
    pub shrink_by: Option<f32>,

    /// Make this layout inline
    ///
    /// The default is `false`
    #[prop_or(false)]
    pub inline: bool,

    /// Reverse the order of the children
    ///
    /// The default is `false`
    #[prop_or(false)]
    pub reverse: bool,

    /// Wrap into another row when there is no more horizontal space.
    ///
    /// The default is `false`
    #[prop_or(false)]
    pub wrap: bool,

    /// Align the children inside this row in main direction (vertically).
    ///
    /// The default is `None`
    #[prop_or_default]
    pub align: Option<Align>,

    /// Align the children inside this row in the cross direction
    /// (horizontally).
    ///
    /// The default is `None`
    #[prop_or_default]
    pub cross_align: Option<CrossAlign>,

    /// Align this row when it's inside another layout, the alignment direction
    /// is relevant to the parent layout direction
    ///
    /// When the parent is `Row` it will align horizontally, when the parent is
    /// `Column` it will align vertically.
    ///
    /// Note: This only works when this `Row` inside another layout (e.g. Row/Column).
    ///
    /// The default is `None`
    #[prop_or_default]
    pub align_self: Option<AlignSelf>,

    /// Gap between children.
    ///
    /// This take `Gap` value, which can take either one value that defines the
    /// gap for both the columns and rows (if there is any), or two values one
    /// for rows and the other for columns.
    ///
    /// The default is `None`
    #[prop_or_default]
    pub gap: Option<Gap>,

    /// Reverse rows if there is more than one column within this `Column`.
    ///
    /// This only works when used with `wrap=true`.
    ///
    /// The default is `false`
    #[prop_or(false)]
    pub reverse_rows: bool,

    /// Align rows in the cross direction (vertically) if there is more than one
    /// row within this `Row`.
    ///
    /// The default is `None`
    #[prop_or_default]
    pub align_rows: Option<AlignRows>,

    /// Overflow behavior for this Row
    ///
    /// By default any child that get oversized will be visible and may overlap
    /// with other UI components. Change this property if you like to make the
    /// content scrollable or make the oversized hidden/cliped.
    ///
    /// The default is `Overflow::visible()`
    #[prop_or(Overflow::visible())]
    pub overflow: Overflow,

    /// Padding for the `Row`
    ///
    /// The default is `None`
    #[prop_or_default]
    pub padding: Option<Padding>,

    /// Margin for the `Row`
    ///
    /// The default is `None`
    #[prop_or_default]
    pub margin: Option<Margin>,

    /// Background for the `Row`
    ///
    /// The default is `None`
    #[prop_or_default]
    pub background: Option<Background>,

    /// Border for the `Row`
    ///
    /// The default is `None`
    #[prop_or_default]
    pub border: Option<Border>,

    /// Shadow for the `Row`
    ///
    /// The default is `None`
    #[prop_or_default]
    pub shadow: Option<Shadow>,
}

/// Row layout component.
///
/// See [row properties docs](crate::row::Props) for more details on how to
/// use them.
///
/// # Usage
///
/// ```rust
/// use yew_layout::{Row, Align};
/// # use yew::prelude::*;
///
/// # fn view() -> Html {
/// html! {
///     <Row align={ Align::Center } wrap=true>
///         { "Row children.." }
///     </Row>
/// }
/// # }
/// ```
#[function_component(Row)]
pub fn row(props: &Props) -> Html {
    let style = Style::default()
        .display(match props.inline {
            true => Display::InlineFlex,
            false => Display::Flex,
        })
        .and_size(|s| {
            s.try_width(props.length.clone())
                .try_min_width(props.min_length.clone())
                .try_max_width(props.max_length.clone())
                .try_height(props.width.clone())
                .try_min_height(props.min_width.clone())
                .try_max_height(props.max_width.clone())
        })
        .flex_direction(match props.reverse {
            true => FlexDirection::RowReverse,
            false => FlexDirection::Row,
        })
        .try_flex_wrap(match (props.wrap, props.reverse_rows) {
            (true, true) => Some(Wrap::WrapReverse),
            (true, false) => Some(Wrap::Wrap),
            _ => None,
        })
        .try_justify_content(props.align)
        .try_align_items(props.cross_align)
        .try_align_content(props.align_rows)
        .try_align_self(props.align_self)
        .try_flex_grow(props.expand_by)
        .try_flex_shrink(props.shrink_by)
        .try_gap(props.gap.clone())
        .try_padding(props.padding.clone())
        .try_margin(props.margin.clone())
        .try_background(props.background.clone())
        .try_border(props.border.clone())
        .try_box_shadow(props.shadow.clone())
        .insert("box-sizing", "border-box")
        .merge(props.overflow);

    html! {
        <div class="yew-layout-row" style={ style.to_string() }>
            { props.children.clone() }
        </div>
    }
}
