use std::any::TypeId;

use crate::*;
use css_style::{prelude::*, AlignSelf};
use yew::prelude::*;

// ======================== Column Component ========================

/// Spacer properties.
///
/// Here you can find all properties that can be used with
/// [Spacer](crate::spacer::Spacer) component.
#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    /// Minimum length for this spacer.
    ///
    /// By default the spacer will take the available space, this property can
    /// only control minimum length for the spacer.
    pub min: Option<Length>,
}

pub struct Spacer {
    parent_ty: ParentType,
}

impl Component for Spacer {
    type Message = ();
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        Spacer {
            // NOTE: not sure if this should be in the view fn instead.
            parent_ty: ParentType::from(ctx.link().get_parent().map(|p| p.get_type_id().clone())),
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let style = style()
            .align_self(AlignSelf::Stretch)
            .flex_grow(1.0)
            .flex_shrink(1.0)
            .and_size(|s| match self.parent_ty {
                ParentType::Column => s.max_height(1.0).try_min_height(props.min.clone()),
                ParentType::Row => s.max_width(1.0).try_min_width(props.min.clone()),
                ParentType::Unknow | ParentType::Nothing => s
                    .max_width(1.0)
                    .max_height(1.0)
                    .try_min_height(props.min.clone())
                    .try_min_width(props.min.clone()),
            })
            .to_string();
        html! {
            <div style={style}></div>
        }
    }
}

// ======================

#[derive(Debug)]
enum ParentType {
    Row,
    Column,
    Unknow,
    Nothing,
}

impl From<Option<TypeId>> for ParentType {
    fn from(source: Option<TypeId>) -> Self {
        let row = TypeId::of::<Row>();
        let column = TypeId::of::<Column>();

        match source {
            Some(val) if val == row => ParentType::Row,
            Some(val) if val == column => ParentType::Column,
            Some(_) => ParentType::Unknow,
            None => ParentType::Nothing,
        }
    }
}
