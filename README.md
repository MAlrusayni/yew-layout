# yew-layout

[![master docs](https://img.shields.io/badge/docs-master-blue.svg)](https://malrusayni.gitlab.io/yew-layout/yew_layout/)
&middot;
[![crate info](https://img.shields.io/crates/v/yew-layout.svg)](https://crates.io/crates/yew-layout)
&middot;
[![pipeline](https://gitlab.com/MAlrusayni/yew-layout/badges/master/pipeline.svg)](https://gitlab.com/MAlrusayni/yew-layout/pipelines)
&middot;
[![rustc version](https://img.shields.io/badge/rustc-stable-green.svg)](https://crates.io/crates/yew-layout)
&middot;
[![unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)

This crate provides you a layouts components based on [**Yew
Framwork**](https://crates.io/crates/yew), those components are used to build
your view. [See API Docs](https://malrusayni.gitlab.io/yew-layout/yew_layout/)

**NOTE:** yew-layout is not (yet) prodction ready but is good for use in side
projects and internal tools.

# Available Layouts

- **Row**: A layout that align it's children horizontally.
- **Column**: A layout that align it's children vertically.

# Features

- **Reasonable Defaults**: All layouts have reasonable default properties
  values, which work very well with most use cases.
- **Rich Properties**: All layouts have rich properties that can be tweaked in
  many different ways to fit other use cases.
- **Consistent Properties**: Layouts almost have the same properties, So if you
  decide to change from `Row` to `Column` just do it and properties would work
  the same, their behavior would change to match the currant layout type!.
- **Powered By Flexbox**: `Row` and `Column` uses Flexbox behind the scene,
  there is no black magic fortunately.

# Goal

The goal for this crate is to provide you a layout types that is used to
layout your views, nothing else.

# Quick Example

```rust
use css_style::{color, unit::px, Background, Border};
use yew::prelude::*;
use yew_layout::{
    Align, AlignRows, Column, CrossAlign, Gap, Length, Margin, Overflow, Padding, Row,
};

#[function_component(App)]
pub fn app() -> Html {
    let border = Border::from(color::named::DARKORCHID).width(px(2)).dashed();
    html! {
        <Column align={ Align::Center }
                cross_align={ CrossAlign::Center }
                gap={ Gap::from(px(8)) }
                width={ Length::from(px(350)) }
                length={ Length::from(px(550)) }
                border={ Border::default().radius(px(8)) }
                background={ Background::from(color::named::SILVER) } >
            <Row align={ Align::Center }
                 align_rows={ AlignRows::Center }
                 wrap=true
                 min_width={ Length::from(px(220)) }
                 gap={ Gap::from(px(12)) }
                 padding={ Padding::default().y(px(12)) }
                 margin={ Margin::default().x(px(20)) }
                 border={ border.clone().top_left(px(8)).top_right(px(8)) }>
                {
                    (0..=11)
                        .into_iter()
                        .map(|i| html!{
                            <div key={i} style={ "background: dodgerblue; height: 40px; width: 40px;" }></div>
                        }).collect::<Html>()
                }
            </Row>
            <Column border={ border.bottom_left(px(8)).bottom_right(px(8)) }
                    overflow={ Overflow::hidden().y_auto_scroll() }
                    padding={ Padding::default().x(px(10)) }>
                <h1>{ "Sub-column with scroll bar" }</h1>
                <p>{ "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.  Donec hendrerit tempor tellus.  Donec pretium posuere tellus.  Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus.  Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.  Nulla posuere.  Donec vitae dolor.  Nullam tristique diam non turpis.  Cras placerat accumsan nulla.  Nullam rutrum.  Nam vestibulum accumsan nisl." }</p>
            </Column>
        </Column>
    }
}
```

this would look like:
![example screenshot](https://gitlab.com/MAlrusayni/yew-layout/-/raw/master/screenshots/example%20-%20simple.png)
